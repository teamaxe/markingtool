//
//  FileIO.h
//  MarkingTool
//
//  Created by tj3-mitchell on 19/05/2013.
//
//

#ifndef __MarkingTool__FileIO__
#define __MarkingTool__FileIO__

#include "../JuceLibraryCode/JuceHeader.h"

class FileIO
{
public:
    static bool saveSettingsFile (ValueTree &treeToSave)
    {
        File settingsFile = getSettingsFile();
        
        if (! settingsFile.existsAsFile()) {
            settingsFile.create();
        }
        
        if (treeToSave.isValid())
        {
            ScopedPointer<XmlElement> settingsXml(treeToSave.createXml());
            if (settingsXml->writeToFile(settingsFile, "")) {
                DBG("XML File written");
                ScopedPointer<XmlElement> xml(treeToSave.createXml());
                if (&xml != nullptr) {
                    DBG(settingsFile.getFullPathName());
                    DBG(xml->createDocument("")); 
                }
                return true;
            }
            else {
                DBG("Error saving settings");
                return false;
            }
        }
        else
            DBG("Settings not valid to save");
        
        return false;
    }
    
    static bool saveSettingsFile (const File& settingsFile, ValueTree &treeToSave)
    {
        if (! settingsFile.existsAsFile()) {
            settingsFile.create();
        }
        
        if (treeToSave.isValid())
        {
            ScopedPointer<XmlElement> settingsXml(treeToSave.createXml());
            if (settingsXml->writeToFile(settingsFile, "")) {
                DBG("XML File written");
                ScopedPointer<XmlElement> xml(treeToSave.createXml());
                if (&xml != nullptr) {
                    DBG(settingsFile.getFullPathName());
                    DBG(xml->createDocument(""));
                }
                return true;
            }
            else {
                DBG("Error saving settings");
                return false;
            }
        }
        else
            DBG("Settings not valid to save");
        
        return false;
    }

    
    static bool loadSettingsFile (ValueTree& treeToFill)
    {
        File settingsFile = getSettingsFile();
        if (settingsFile.existsAsFile())
        {
            XmlDocument settingsDoc (settingsFile);
            ScopedPointer<XmlElement> settingsXML(settingsDoc.getDocumentElement());
            
            treeToFill = ValueTree::fromXml(*settingsXML);
            
            return treeToFill.isValid();
        }
        
        return false;
    }
    
    static bool loadSettingsFile (const File& settingsFile, ValueTree& treeToFill)
    {
        if (settingsFile.existsAsFile())
        {
            XmlDocument settingsDoc (settingsFile);
            ScopedPointer<XmlElement> settingsXML(settingsDoc.getDocumentElement());
            
            treeToFill = ValueTree::fromXml(*settingsXML);
            
            return treeToFill.isValid();
        }
        
        return false;
    }
    
private:
    static File getSettingsFile()
    {
        File resourcesFolder(File::getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getChildFile("Settings"));
        
        if (resourcesFolder.exists() == false)
            resourcesFolder.createDirectory();
        
        File settingsFile(resourcesFolder.getChildFile("settings.xml"));
        
        return settingsFile;
    }
};

#endif /* defined(__MarkingTool__FileIO__) */
