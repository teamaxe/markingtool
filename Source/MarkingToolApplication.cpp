/*
 *  MarkingToolApplication.cpp
 *  sdaProj
 *
 */

#include "MarkingToolApplication.h"

MarkingToolApplication::MarkingToolApplication() : markingToolWindow (nullptr)
{
    
}

MarkingToolApplication::~MarkingToolApplication()
{
    if (FileIO::saveSettingsFile (modelTree) == false)
        DBG("Error Saving Settings File\n");
}

//==============================================================================
void MarkingToolApplication::initialise(const String& commandLine)
{
    //load the model from file.
    if (FileIO::loadSettingsFile (modelTree) == false)
    {
        modelTree = MarkingTool::createDefaultModel();
    }
    
    markingToolWindow = new MarkingToolWindow (modelTree);
}

void MarkingToolApplication::shutdown()
{

	deleteAndZero (markingToolWindow);
}

//==============================================================================
void MarkingToolApplication::systemRequestedQuit()
{
	quit();
}

//==============================================================================
const String MarkingToolApplication::getApplicationName()
{
	// The name for the application
	return ProjectInfo::projectName;
}

const String MarkingToolApplication::getApplicationVersion()
{

	
	return ProjectInfo::versionString;;
}

bool MarkingToolApplication::moreThanOneInstanceAllowed()
{
	
	return true;
}

void MarkingToolApplication::anotherInstanceStarted(const String& commandLine)
{
	
}

//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION(MarkingToolApplication)