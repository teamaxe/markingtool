//
//  CommentGroupsTabbedComponent.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 08/05/2013.
//
//

#include "CommentGroupsTabbedComponent.h"

// This is a small cross button that is put inside one of the tabs. You can
// use this technique to create things like "close tab" buttons, etc.
class CustomTabButton  : public Component
{
public:
    CustomTabButton()
    {
        setSize (20, 20);
    }
    
    void paint (Graphics& g)
    {
        if (isMouseOver())
        {
            g.setColour (Colours::grey);
            Path p;
            p.addEllipse(0, 0, getWidth(), getWidth());
            g.fillPath (p, RectanglePlacement (RectanglePlacement::centred)
                        .getTransformToFit (p.getBounds(), getLocalBounds().reduced (2).toFloat()));
        }
        g.setColour (Colours::red);
        g.drawFittedText("x", 0, 0, getWidth(), getHeight(), Justification::centred, 1);
    }
    
    
    
    void mouseDown (const MouseEvent& event)
    {
        CommentGroupsTabbedComponent* ctc = findParentComponentOfClass<CommentGroupsTabbedComponent>();
        TabBarButton* tbb = findParentComponentOfClass<TabBarButton>();
        ctc->removeCommentGroup(tbb->getIndex());
        
    }
};


CommentGroupsTabbedComponent::CommentGroupsTabbedComponent (ValueTree& tree) :    TabbedComponent (TabbedButtonBar::TabsAtTop),
commentGroupsTree (CommentGroups::getOrCreateCommentGroupsTree (tree))
{
    commentGroupsTree.addListener(this);
    refreshFromTree();
}

void CommentGroupsTabbedComponent::refreshFromTree()
{
    clearTabs();
    const int numCommentGroups = commentGroupsTree.getNumChildren();
    
    for (int i = 0; i < numCommentGroups; i++)
    {
        addCommentGroup();
    }
    
}

void CommentGroupsTabbedComponent::addCommentGroup()
{
    const int index = getNumTabs();
    ValueTree CommentGroupTree (commentGroupsTree.getChild (index));
    CommentGroupComponent* newCommentGroup = new CommentGroupComponent (CommentGroupTree);
    newCommentGroup->addActionListener(this);
    addTab (CommentGroupTree.getProperty(CommentGroup::Ids::title, "ErrorReadingTree"), Colours::white, newCommentGroup, true);
    getTabbedButtonBar().getTabButton (getNumTabs()-1)->setExtraComponent (new CustomTabButton(), TabBarButton::afterText);
}


void CommentGroupsTabbedComponent::removeCommentGroup (const int index)
{
    CommentGroup::removeCommentGroup(commentGroupsTree, index);
}

void CommentGroupsTabbedComponent::popupMenuClickOnTab (int tabIndex, const String &tabName)
{
    PopupMenu m;
    m.addItem (1, "Rename");
    
    const int result = m.show();
    
    if (result == 1)
    {
        AlertWindow w ("Rename..",
                       "Enter new name for tab",
                       AlertWindow::QuestionIcon);
        
        w.addTextEditor ("text", "enter some text here", "text field:");
        
        w.addButton ("ok",     1, KeyPress (KeyPress::returnKey, 0, 0));
        w.addButton ("cancel", 0, KeyPress (KeyPress::escapeKey, 0, 0));
        
        if (w.runModalLoop() != 0) // is they picked 'ok'
        {
            String text = w.getTextEditorContents ("text");
            commentGroupsTree.getChild (tabIndex).setProperty (CommentGroup::Ids::title, text, nullptr);
            
        }
    }
}

void CommentGroupsTabbedComponent::mouseUp (const MouseEvent& event)
{
    if (event.mods.isRightButtonDown())
    {
        PopupMenu m;
        m.addItem (1, "New tab");
        
        const int result = m.show();
        if (result == 1)
        {
            CommentGroup::addCommentGroup(commentGroupsTree);
        }
    }
}

void CommentGroupsTabbedComponent::actionListenerCallback (const String& message)
{
    sendActionMessage (message);
}

//==============================================================================
void CommentGroupsTabbedComponent::valueTreePropertyChanged (ValueTree& tree, const Identifier& property)
{
    if (tree.getType() == CommentGroup::Ids::commentGroup)
    {
        int tabIndex = CommentGroup::getCommentGroupIndex (tree);
        setTabName (tabIndex, tree.getProperty(property, "Comment"));
    }
    DBG ("CommentGroupsTabbedComponent::valueTreePropertyChanged");
}
void CommentGroupsTabbedComponent::valueTreeChildAdded (ValueTree& parent, ValueTree& addedChild)
{
    //if its not a subtree!
    DBG (addedChild.getType().toString() << "");
    if (addedChild.getType() == CommentGroup::Ids::commentGroup)
        addCommentGroup ();
    
    DBG ("CommentGroupsTabbedComponent::valueTreeChildAdded");
}
void CommentGroupsTabbedComponent::valueTreeChildRemoved (ValueTree& parent, ValueTree& removedChild)
{
    if (parent == commentGroupsTree)
    {
        for (int i = 0; i < getNumTabs(); i++)
        {
            CommentGroupComponent* commentGroupComponent = dynamic_cast<CommentGroupComponent*>(getTabContentComponent(i));
            if (commentGroupComponent != nullptr)
            {
                if (commentGroupComponent->getCommentGroupTree() == removedChild)
                {
                    removeTab (i);
                    break;
                }
            }
        }
    }
    DBG ("CommentGroupsTabbedComponent::valueTreeChildRemovedd" << "");
}
void CommentGroupsTabbedComponent::valueTreeChildOrderChanged (ValueTree& movedParent)
{
    DBG ("CommentGroupsTabbedComponent::valueTreeChildOrderChanged");
}
void CommentGroupsTabbedComponent::valueTreeParentChanged (ValueTree& tree)
{
    DBG ("CommentGroupsTabbedComponent::valueTreeParentChanged");
}