//
//  CommentGroupComponent.h
//  MarkingTool
//
//  Created by tj3-mitchell on 06/05/2013.
//
//

#ifndef __MarkingTool__CommentGroupComponent__
#define __MarkingTool__CommentGroupComponent__

#include "../JuceLibraryCode/JuceHeader.h"
#include "../model/Comment.h"
#include "../model/CommentGroup.h"
#include "CommentComponent.h"

//Change to CommentGroupComponent

class CommentGroupComponent :  public Component,
                           public CommentComponent::Listener,
                           public ActionBroadcaster,
                           private ValueTree::Listener
{
public:
    CommentGroupComponent(const ValueTree& commentGroupTree_);
    ~CommentGroupComponent();
    void refreshFromTree();
    ValueTree& getCommentGroupTree();
    void resized();
    void messsagePosted (const String& text);
    void addButtonPressed (CommentComponent* source);
    void removeButtonPressed (CommentComponent* source);

    //==============================================================================
    void valueTreePropertyChanged (ValueTree& tree, const Identifier& property);
    void valueTreeChildAdded (ValueTree& parent, ValueTree& addedChild);
    void valueTreeChildRemoved (ValueTree& parent, ValueTree& removedChild);
    void valueTreeChildOrderChanged (ValueTree& movedParent);
    void valueTreeParentChanged (ValueTree& tree);
    
    
private:

    OwnedArray<CommentComponent> commentComponents;
    ValueTree commentGroupTree;
};

#endif /* defined(__MarkingTool__CommentGroupComponent__) */
