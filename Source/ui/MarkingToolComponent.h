/*
 *  MarkingToolComponent.h
 *  sdaProj
 *
 */

#ifndef H_MarkingToolComponent
#define H_MarkingToolComponent

#include "../JuceLibraryCode/JuceHeader.h"
#include "CommentGroupsTabbedComponent.h"
#include "../model/MarkingTool.h"

class MarkingToolComponent  :   public Component,
                                private ActionListener,
                                public MenuBarModel
{	
public:
	//==============================================================================
	MarkingToolComponent(ValueTree& model_);
	~MarkingToolComponent();
    void refreshFromTree();
	//==============================================================================
	void resized();
	void paint (Graphics &g);
    void actionListenerCallback (const String& message);
    //menu
    StringArray getMenuBarNames();
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName);
    void menuItemSelected (int menuItemID, int topLevelMenuIndex);
	//==============================================================================
private:
    enum Menus
    {
        FileMenu,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        FileOpenItem = 1,
        FileSaveItem,
        
        NumFileItems
    };
	//==============================================================================
    
    TextEditor comment;
    ValueTree model;
    ScopedPointer<CommentGroupsTabbedComponent>  tabs;
    
};




#endif // H_MarkingToolComponent