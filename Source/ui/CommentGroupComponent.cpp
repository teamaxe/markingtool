//
//  CommentTab.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 06/05/2013.
//
//

#include "CommentGroupComponent.h"

CommentGroupComponent::CommentGroupComponent(const ValueTree& commentGroupTree_) : commentGroupTree (commentGroupTree_)
{
    
    commentGroupTree.addListener(this);
    refreshFromTree();
}

CommentGroupComponent::~CommentGroupComponent()
{
    
}

void CommentGroupComponent::refreshFromTree()
{
    for (int i = 0; i < commentComponents.size(); i++)
    {
        removeChildComponent(commentComponents[i]);
    }
    
    const int numCommentGroups = CommentGroup::getNumberOfComments (commentGroupTree);
    
    for (int i = 0; i < numCommentGroups; i++)
    {
        const int index = commentComponents.size();
        ValueTree commentTree (commentGroupTree.getChild(index));
        CommentComponent* newComment = new CommentComponent (commentTree);
        addAndMakeVisible(newComment);
        newComment->addListener(this);
        commentComponents.add(newComment);
    }
    resized();
}

ValueTree& CommentGroupComponent::getCommentGroupTree()
{
    return commentGroupTree;
}

void CommentGroupComponent::resized()
{
    int commentHeight = jmin(getHeight() / commentComponents.size(), 30);
    commentComponents.getFirst()->setBounds (0, 0, getWidth(), commentHeight);
    
    for (int i = 1; i < commentComponents.size(); i++)
    {
        commentComponents[i]->setBounds(commentComponents[i-1]->getBounds().translated(0, commentHeight));
    }
}

void CommentGroupComponent::messsagePosted (const String& text)
{
    sendActionMessage(text);
}

void CommentGroupComponent::addButtonPressed (CommentComponent* source)
{
    int index = commentComponents.indexOf(source);
    Comment::addComment (commentGroupTree, index + 1); //insert before the one that appeared
}

void CommentGroupComponent::removeButtonPressed (CommentComponent* source)
{
    if (commentComponents.size() == 1)
        return;
    
    int index = commentComponents.indexOf(source);
    Comment::removeComment(commentGroupTree, index);
}


//==============================================================================
void CommentGroupComponent::valueTreePropertyChanged (ValueTree& tree, const Identifier& property)
{
    DBG ("CommentGroupComponent::valueTreePropertyChanged");
}
void CommentGroupComponent::valueTreeChildAdded (ValueTree& parent, ValueTree& addedChild)
{
    if (parent == commentGroupTree)
    {
        commentComponents.clear();
        refreshFromTree();
    }
    DBG ("CommentGroupComponent::valueTreeChildAdded");
}
void CommentGroupComponent::valueTreeChildRemoved (ValueTree& parent, ValueTree& removedChild)
{
    if (parent == commentGroupTree)
    {
        commentComponents.clear();
        refreshFromTree();
    }
    DBG ("CommentGroupComponent::valueTreeChildRemovedd");
}
void CommentGroupComponent::valueTreeChildOrderChanged (ValueTree& movedParent)
{
    DBG ("CommentGroupComponent::valueTreeChildOrderChanged");
}
void CommentGroupComponent::valueTreeParentChanged (ValueTree& tree)
{
    DBG ("CommentGroupComponent::valueTreeParentChanged");
}