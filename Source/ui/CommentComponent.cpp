//
//  CommentComponent.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 25/04/2013.
//
//

#include "CommentComponent.h"
#include "../model/Comment.h"

CommentComponent::CommentComponent(ValueTree commentTree_) : commentTree (commentTree_)
{
    
    postButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    postButton.addListener (this);
    addAndMakeVisible (&postButton);
    
    minusButton.setButtonText ("-");
    minusButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    minusButton.addListener (this);
    addAndMakeVisible (&minusButton);
    
    plusButton.setButtonText ("+");
    plusButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    plusButton.addListener (this);
    addAndMakeVisible (&plusButton);
    
    comment.setMultiLine (true);
    comment.getTextValue().referTo(commentTree.getPropertyAsValue(Comment::Ids::text, nullptr));
    addAndMakeVisible(&comment);
}
CommentComponent::~CommentComponent()
{
	
}


void CommentComponent::addListener(Listener* const listenerToAdd)
{
    if (listenerToAdd != nullptr)
        listeners.add(listenerToAdd);
}


void CommentComponent::removeListener(Listener* const listenerToRemove)
{
    if (listenerToRemove != nullptr)
        listeners.remove(listenerToRemove);
}

//==============================================================================
void CommentComponent::buttonClicked (Button* button)
{
    if (button == &postButton)
    {
        listeners.call(&Listener::messsagePosted, comment.getText());
    }
    else if (button == &plusButton)
    {
        //signal to remove this button
        listeners.call(&Listener::addButtonPressed, this);
    }
    else if (button == &minusButton)
    {
        //signal to add a button below this one.
        listeners.call(&Listener::removeButtonPressed, this);
    }
}

void CommentComponent::resized()
{
    int x = getWidth();
    int y = getHeight();
    
    postButton.setBounds(0, 0, y, y);
    comment.setBounds(postButton.getRight(), 0, x - 3 * y, y);
    minusButton.setBounds(comment.getRight(), 0, y, y);
    plusButton.setBounds(minusButton.getBounds().translated(y, 0));
}

void CommentComponent::paint (Graphics &g)
{
    
}
