/*
 *  MarkingToolComponent.cpp
 *  sdaProj
 *
 */

#include "MarkingToolComponent.h"
#include "FileIO.h"


MarkingToolComponent::MarkingToolComponent(ValueTree& model_) : model (model_)
{
    refreshFromTree();
    
    comment.setMultiLine(true);
    comment.setReturnKeyStartsNewLine(true);
    addAndMakeVisible(&comment);
    
    
}
MarkingToolComponent::~MarkingToolComponent()
{
	
}

void MarkingToolComponent::refreshFromTree()
{
    if (tabs.get() != nullptr)
    {
        tabs->removeActionListener(this);
        removeChildComponent(tabs);
    }
    tabs = new CommentGroupsTabbedComponent (model);
    addAndMakeVisible (tabs);
    tabs->setTabBarDepth (25);
    tabs->setCurrentTabIndex (0);
    tabs->addActionListener(this);
    resized();
}

//==============================================================================
void MarkingToolComponent::resized()
{
    int commetEndHeight = 200;
    
    tabs->setBounds (0, 0, getWidth(), getHeight() - commetEndHeight);
	comment.setBounds (0, getHeight() - commetEndHeight, getWidth(), commetEndHeight);
}

void MarkingToolComponent::paint (Graphics &g)
{

}

void MarkingToolComponent::actionListenerCallback (const String& message)
{
    comment.insertTextAtCaret(message + " ");
}

StringArray MarkingToolComponent::getMenuBarNames()
{
    const char* const names[] = {"File", nullptr};
    return StringArray (names);
}

PopupMenu MarkingToolComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == FileMenu)
    {
        menu.addItem(FileOpenItem, "Open");
        menu.addItem(FileSaveItem, "Save");
    }
    return menu;
}

void MarkingToolComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == FileOpenItem)
        {
            FileChooser fc ("Choose a file to open...",
                            File::getCurrentWorkingDirectory(),
                            "*.xml",
                            true);
            
            if (fc.browseForFileToOpen())
            {
                FileIO::loadSettingsFile (fc.getResult(), model);
                refreshFromTree();
            }
        }
        else if (menuItemID == FileSaveItem)
        {
            FileChooser fc ("Choose a file to save...",
                            File::getCurrentWorkingDirectory(),
                            "*.xml",
                            true);
            
            if (fc.browseForFileToSave (true))
            {
                File chosenFile = fc.getResult();
                
                FileIO::saveSettingsFile (chosenFile, model);
            }
        }
    }
}