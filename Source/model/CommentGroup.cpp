//
//  CommentGroup.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 11/05/2013.
//
//

#include "CommentGroup.h"
#include "CommentGroups.h"
#include "Comment.h"
#include "MarkingTool.h"

const Identifier CommentGroup::Ids::commentGroup          ("COMMENTGROUP");
const Identifier CommentGroup::Ids::title                  ("text");

ValueTree CommentGroup::createCommentGroup()
{
    std::cout << "CommentGroupTree Created\n";
    ValueTree CommentGroupTree (Ids::commentGroup);
    
    CommentGroupTree.setProperty(Ids::title, "Comments", nullptr);
    
    Comment::addComment(CommentGroupTree, -1);//minus 1 means insert at the end
    
    return CommentGroupTree;
}

ValueTree CommentGroup::addCommentGroup (ValueTree& commentGroupsTree)
{
    
    if (commentGroupsTree.isValid() == false && commentGroupsTree.getType() != CommentGroups::Ids::commentGroups)
    {
        return ValueTree::invalid;
    }
    
    ValueTree newComment (createCommentGroup());
    commentGroupsTree.addChild (newComment, -1, nullptr);
    
    return newComment;
}

//ValueTree CommentGroup::getCommentGroupTree (const ValueTree& commentGroupsTree)
//{
//    if (commentGroupsTree.isValid() == false)
//    {
//        return ValueTree::invalid;
//    }
//    
//    CommentGroupTree = createCommentGroup();
//    model.addChild (CommentGroupTree, 0, nullptr);
//    
//    return CommentGroupTree;
//}


//ValueTree CommentGroup::addCommentGroup (const ValueTree& tree)
//{
//    ValueTree CommentGroupsTree (CommentGroups::getOrCreateCommentGroupsTree (tree));
//    
//    if (! CommentGroupsTree.isValid())
//        return ValueTree::invalid;
//    
//    ValueTree newCommentsTree (createCommentGroup());
//    CommentGroupsTree.addChild (newCommentsTree, -1, nullptr);
//    
//    return CommentGroupsTree;
//}

//int CommentGroup::getNumComments (const ValueTree& CommentGroupTree)
//{
//    //ValueTree CommentGroupTree (getOrCreateCommentGroupTree (tree));
//    
//    return CommentGroupTree.isValid() ? CommentGroupTree.getNumChildren() : -1;
//}

bool CommentGroup::removeCommentGroup (const ValueTree& tree, const int childIndex)
{
    ValueTree CommentGroupsTree (CommentGroups::getOrCreateCommentGroupsTree (tree));
    
    if (! CommentGroupsTree.isValid())
        return true;
    
    CommentGroupsTree.removeChild (childIndex, nullptr);
    
    return false;
}

int CommentGroup::getCommentGroupIndex (const ValueTree& CommentGroupsTree)
{
    return CommentGroupsTree.getParent().indexOf (CommentGroupsTree);
}

int CommentGroup::getNumberOfComments (const ValueTree& commentGroupTree)
{
    return commentGroupTree.getNumChildren();
}

//ValueTree CommentGroup::getComment (const ValueTree& commentGroupTree, int index)
//{
//    //ValueTree CommentGroupTree (getOrCreateCommentGroupTree (tree));
//
//    return commentGroupTree.getChild (index);
//}
