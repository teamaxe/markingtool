//
//  Comment.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 11/05/2013.
//
//

#include "Comment.h"
#include "CommentGroup.h"

const Identifier Comment::Ids::comment         ("COMMENT");
const Identifier Comment::Ids::text            ("text");

ValueTree Comment::createComment()
{
    ValueTree commentTree (Ids::comment);
    
    commentTree.setProperty (Ids::text, String::empty, nullptr);
    
    return commentTree;
}

ValueTree Comment::addComment (ValueTree& commentGroupTree, const int insertIndex)
{
    
    if (commentGroupTree.isValid() == false && commentGroupTree.getType() != CommentGroup::Ids::commentGroup)
    {
        return ValueTree::invalid;
    }
    
    ValueTree newComment (createComment());
    commentGroupTree.addChild (newComment, insertIndex, nullptr);
    
    return newComment;
}

bool Comment::removeComment (ValueTree& commentGroupTree, const int commentIndex)
{
    if (commentGroupTree.isValid() == false && commentGroupTree.getType() != CommentGroup::Ids::commentGroup)
    {
        return true;
    }
    
    commentGroupTree.removeChild (commentIndex, nullptr);
    
    return false;
}


//int Comment::getCommentIndex (const ValueTree& commentTree)
//{
//    return commentTree.getParent().indexOf (commentTree);
//}
