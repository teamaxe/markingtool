//
//  CommentGroup.h
//  MarkingTool
//
//  Created by tj3-mitchell on 11/05/2013.
//
//

#ifndef __MarkingTool__File__
#define __MarkingTool__File__

#include "../JuceLibraryCode/JuceHeader.h"

class CommentGroup
{
public:
    
    //==============================================================================
    struct Ids
    {
        static const Identifier commentGroup;
        static const Identifier title;
    };
    //==============================================================================
    
    static ValueTree addCommentGroup (ValueTree& commentGroupsTree);
    
//    static ValueTree getOrCreateCommentGroupTree (const ValueTree& tree);
    
//    static ValueTree addCommentGroup (const ValueTree& tree);
    
//    static int getNumComments (const ValueTree& tree);
    
    static bool removeCommentGroup (const ValueTree& commentGroupsTree, const int childIndex);
    
    static int getCommentGroupIndex (const ValueTree& commentGroupTree);
    
    static int getNumberOfComments (const ValueTree& commentGroupTree);
    
//    static ValueTree getComment (const ValueTree& tree, int index);
    
private:
    static ValueTree createCommentGroup();
    //==============================================================================
    CommentGroup();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CommentGroup)
};


#endif /* defined(__MarkingTool__File__) */
