//
//  Comment.h
//  MarkingTool
//
//  Created by tj3-mitchell on 11/05/2013.
//
//

#ifndef __MarkingTool__Comment__
#define __MarkingTool__Comment__

#include "../JuceLibraryCode/JuceHeader.h"

class Comment
{
public:
    
    //==============================================================================
    struct Ids
    {
        static const Identifier comment;
        static const Identifier text;
    };
    
    //==============================================================================
    
    static ValueTree addComment (ValueTree& tree, const int insertIndex);
    static bool removeComment (ValueTree& tree, const int removeIndex);
//    static int getCommentIndex (const ValueTree& deckTree);
    
private:
    static ValueTree createComment();
    //==============================================================================
    Comment();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Comment)
};

#endif /* defined(__MarkingTool__Comment__) */
