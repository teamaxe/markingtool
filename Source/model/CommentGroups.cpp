//
//  CommentTabModel.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 07/05/2013.
//
//

#include "CommentGroups.h"
#include "CommentGroup.h"
#include "MarkingTool.h"

const Identifier CommentGroups::Ids::commentGroups   ("COMMENTGROUPS");

//==============================================================================
ValueTree CommentGroups::createCommentGroups()
{
    ValueTree CommentGroupsTree (Ids::commentGroups);
    
    CommentGroup::addCommentGroup(CommentGroupsTree);
    
    return CommentGroupsTree;
}

ValueTree CommentGroups::getOrCreateCommentGroupsTree (const ValueTree& tree)
{
    ValueTree model (MarkingTool::getModelTree (tree));
    ValueTree CommentGroupsTree;
    
    if (! model.isValid())
        return ValueTree::invalid;
    
    CommentGroupsTree = model.getChildWithName (Ids::commentGroups);
    
    if (! CommentGroupsTree.isValid())
    {
        CommentGroupsTree = createCommentGroups();
        model.addChild (CommentGroupsTree, 0, nullptr);
    }
    
    return CommentGroupsTree;
}

//int CommentGroups::getNumCommentGroups (const ValueTree& tree)
//{
//    ValueTree CommentGroupsTree (getOrCreateCommentGroupsTree (tree));
//    
//    return CommentGroupsTree.isValid() ? CommentGroupsTree.getNumChildren() : -1;
//}
//
//
//ValueTree CommentGroups::getCommentGroup (const ValueTree& tree, int index)
//{
//    ValueTree CommentGroupsTree (getOrCreateCommentGroupsTree (tree));
//    
//    return CommentGroupsTree.getChild (index);
//}
//
//int CommentGroups::getCommentGroupIndex (const ValueTree& commentGroupTree)
//{
//    return commentGroupTree.getParent().indexOf (commentGroupTree);
//}
